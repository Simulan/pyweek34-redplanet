# The Martian Thirst

A game about water under Mars, and taking the fate of a colony world into your own hands.

[Click here to go to the PyWeek entry page.](https://pyweek.org/e/GMars/) - both the source and binary builds are available there.

The source code archive is at https://codeberg.org/Simulan/pyweek34-redplanet

## Instructions

To run the game from source, make sure you have a recent version of Panda3D
installed:

```
python -m pip install -U panda3d
```

Then, type `python main.py` to run the game.  If you downloaded a deployed
build instead, just double-click `run_game`.

## Controls

Use A and D to move left and right, and hold W and S to hug the walls.  On
keyboard layouts other than QWERTY, the equivalent keys on those positions are
accepted.  Use the spacebar to shoot.

## Credits

Lovingly created by

```
Entikan
Simulan
Schwarzbaer
  &
rdb, who objected fruitlessly to his outrageous in-game credit
```
