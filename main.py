from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence, Func, Wait
from panda3d.core import load_prc_file_data
from panda3d.core import TransparencyAttrib, SamplerState, Texture
from panda3d.core import NodePath, AmbientLight, Vec3
from panda3d.core import LineSegs
from panda3d.core import TextNode
import sys
import random

load_prc_file_data("",
"""
window-title The Martian Thirst
win-size 1280 720
framebuffer-srgb true
textures-power-2 none
texture-minfilter nearest
texture-magfilter nearest
text-scale-factor 1
hardware-animated-vertices true
simple-shaders-only false
cull-bin true
""")

END_OF_LEVEL = 14
PLAYER_WALK_SPEED_NORMAL = 0.75
PLAYER_WALK_SPEED_FIRING = 0.5
ENEMY_SPREAD = 50 # total width along which enemies spawn, not same units as end of level :-|
ENEMY_OFFSET = 2 # length before encountering an enemy
ENEMY_COUNT = 21
ENEMY_MINCHANCE = 0.3 # minimum chance an enemy appears in a slot (beg of level)
ENEMY_MAXCHANCE = 0.8 # maximum chance an enemy appears in a slot (end of level)
ENEMY_AGGRO_DIST = 5


class Setup:
    def cancel_escape_handler(task):
        Movement.text_1.set_text("")
        base.accept_once('escape', Setup.escape_handler)
        return task.done

    def escape_handler():
        Movement.text_1.set_text("Press escape again to quit")
        Movement.text_1_node.set_color(.5,.5,.5,1)
        base.accept_once('escape', sys.exit, [0])
        base.taskMgr.do_method_later(1.0, Setup.cancel_escape_handler, 'cancel_escape_handler')

    base = ShowBase()
    base.linesegs = LineSegs('linesegs')
    base.linesegs.set_color(.5,.1,.1,0.8)
    base.cam.set_pos(0,0,0)
    base.disable_mouse()
    base.main_song = None
    base.accept_once('escape', escape_handler)
    base.set_background_color(0,0,0,1)
    base.sfxManagerList[0].set_concurrent_sound_limit(5)


class Backgrounds:

    facility_backgrounds = []
    quarters_backgrounds = []
    cave_backgrounds = []

    blackouts = [
        base.loader.load_model('assets/test/100m_card_black.bam'),
        base.loader.load_model('assets/test/100m_card_black.bam'),
    ]
    for b, blackout in enumerate(blackouts):
        blackout.set_color(.01,0,0,1)
        blackout.reparent_to(base.render)
        blackout.set_hpr(90,90,0)
        blackout.set_pos(0,2.3,50.8)
        if b:
            blackout.set_pos(0,2.3,-50.8)


class SetupBackgrounds:
    def setup_bg(level):
        if level == 'facility':
            for i in range(5):
                bg = base.loader.load_model('assets/bam/100m_facility_L{}.bam'.format(5-i))
                bg.reparent_to(base.render)
                # set the offset from the camera to create a letterbox look
                bg.set_y(2.25+(0.01*i))
                bg.set_x(40)
                # correct model HPR and scale from the gltf conversion
                # sx = 1 sy = 0.0183105 is equivalent to the 75/4096
                # 150 x 8192 scale ratio given a square input card
                bg.set_scale(1,.0183105,1)
                bg.set_hpr(90,90,0)
                for t in bg.find_all_textures():
                    t.set_magfilter(SamplerState.FT_nearest)
                bg.set_transparency(TransparencyAttrib.M_dual)
                Backgrounds.facility_backgrounds.append(bg)
        if level == 'quarters':
            bg = base.loader.load_model('assets/bam/100m_quarters_L1.bam')
            bg.reparent_to(base.render)
            # set the offset from the camera to create a letterbox look
            bg.set_y(2.251)
            bg.set_x(40)
            # correct model HPR and scale from the gltf conversion
            # sx = 1 sy = 0.0183105 is equivalent to the 75/4096
            # 150 x 8192 scale ratio given a square input card
            bg.set_scale(1,.0183105,1)
            bg.set_hpr(90,90,0)
            for t in bg.find_all_textures():
                t.set_magfilter(SamplerState.FT_nearest)
            # the npcs will inherit this transparency layer when they are
            # wrt_reparent'd so it has to be on even if only 1 bg layer
            bg.set_transparency(TransparencyAttrib.M_dual)
            Backgrounds.quarters_backgrounds.append(bg)
        if level == 'cave':
            for i in range(4):
                bg = base.loader.load_model('assets/bam/100m_cave_L{}.bam'.format(4-i))
                bg.reparent_to(base.render)
                # set the offset from the camera to create a letterbox look
                bg.set_y(2.25+(0.01*i))
                bg.set_x(40)
                # correct model HPR and scale from the gltf conversion
                # sx = 1 sy = 0.0183105 is equivalent to the 75/4096
                # 150 x 8192 scale ratio given a square input card
                bg.set_scale(1,.0183105,1)
                bg.set_hpr(90,90,0)
                for t in bg.find_all_textures():
                    t.set_magfilter(SamplerState.FT_nearest)
                bg.set_transparency(TransparencyAttrib.M_dual)
                Backgrounds.cave_backgrounds.append(bg)

    # start the first level
    setup_bg('facility')

class Movement:

    base.key_map = {"left": 0, "right": 0, "up": 0, "down": 0, "run": 0, "jump": 0}

    def set_key(key, value):
        base.key_map[key] = value

    # define button map
    for button, key in zip(
        ["a","d","w","s"], ["left", "right", "up", "down"]
    ):
        base.accept(button, set_key, [key, 1])
        base.accept(button+"-up", set_key, [key, 0])

    remap = base.win.get_keyboard_map()
    for button, key in zip(
        ["a","d","w","s"], ["left", "right", "up", "down"]
    ):
        button = remap.get_mapped_button(button)
        if button:
            base.accept(button.name, set_key, [key, 1])
            base.accept(button.name+"-up", set_key, [key, 0])

    # add movement instructions
    text_1 = TextNode('text_1_node')
    level_text = 'Level 1: Facility'
    text_1.set_text('')
    text_1_node = base.a2dTopLeft.attach_new_node(text_1)
    text_1_node.set_scale(0.04)
    text_1_node.set_pos(.05,0,-.1)

    # add PC health info
    text_2 = TextNode('text_2_node')
    health_text = "100"
    text_2.set_text("Health: " + health_text)
    text_2_node = base.a2dTopLeft.attach_new_node(text_2)
    text_2_node.set_scale(0.05)
    text_2_node.set_pos(.05,0,-.21)
    text_2_node.hide()


def play_sfx(sound, playrate=1):
    sfx = base.loader.load_sfx("assets/sfx/{}.ogg".format(sound))
    sfx.set_play_rate(playrate)
    sfx.play()

def play_song(sound):
    if base.main_song:
        base.main_song.stop()
    base.main_song = base.loader.load_music("assets/music/{}.ogg".format(sound))
    base.main_song.set_loop(True)
    base.main_song.play()


def play_animation(actor, animation, loop=False, stance=0):
    if animation[0] == "_":
        animation = ("center","top","bottom")[stance]+animation
    actor.set_blend(False)
    if not actor.get_current_anim() == animation:
        if loop:
            actor.loop(animation)
        else:
            actor.play(animation)

def has_blend(actor, animation):
    ctrl = actor.get_anim_control(animation)
    if ctrl.is_playing():
        return True

def play_animation_blend(actor, animations=[("animation_name",0.1,False)], stance=0):
    actor.set_blend(True)
    for animation in animations:
        if animation[0][0] == "_":
            animation[0] = ("center","top","bottom")[stance]+animation[0]
        actor.set_control_effect(animation[0], animation[1])
        if not has_blend(actor, animation[0]):
            if animation[2]:
                actor.loop(animation[0])
            else:
                actor.play(animation[0])

def npc_trace(shooter_quad, dying_quad, n_char, side):
    npc_trace = LineSegs('npc_trace')
    npc_trace.set_color(0.3,0.2,0.9,0.5)

    f = shooter_quad.get_pos(base.render)

    if n_char.get_h() == 80:
        f = Vec3(f[0]+.28,f[1],f[2]+2)
        if dying_quad is None:
            t = Vec3(200,2.2,random.uniform(-15,15))
    else:
        f = Vec3(f[0]-.28,f[1],f[2]+2)
        if dying_quad is None:
            t = Vec3(-200,2.2,random.uniform(-15,15))

    if dying_quad is not None:
        t = dying_quad.get_pos(base.render)
        t = Vec3(t[0],t[1],t[2]+3)
        # give the npc some odds of hitting the player on target
        hit_chance = random.choice([0,10])

        t = Vec3(t[0]-(hit_chance*side),t[1],t[2]+random.uniform(-.25,.25))

        if hit_chance == 0:
            PlayerCharacter.pc_hit_total += 50
            hits = PlayerCharacter.pc_hit_total
            health = int(Movement.health_text)
            PlayerCharacter.p_char.hide(); PlayerCharacter.p_char = PlayerCharacter.p_char_hurt;PlayerCharacter.p_char.show()

            if health < 70 >= 25:
                Movement.text_2_node.set_color(1,0,0,1)

            Movement.health_text = str(health - hits)

    npc_trace.move_to(f)
    npc_trace.draw_to(t)
    npc_trace.set_thickness(8)
    line = base.render.attach_new_node(npc_trace.create())
    line.set_transparency(True)

    def fade_line(line, task):
        alpha = line.get_color_scale()[3]
        if alpha <= 0:
            line.remove_node()
            return task.done
        else:
            line.set_alpha_scale(alpha-5*base.clock.dt)
            return task.cont
    base.task_mgr.add(fade_line, extraArgs=[line], appendTask=True)


class PlayerCharacter:

    p_buffer = base.win.make_texture_buffer('player_buff', 512, 512)
    p_root = NodePath('player_root')

    p_cam = base.make_camera(p_buffer)
    p_cam.set_pos(0,-9.5,0)
    p_cam.node().get_lens().set_focal_length(10)
    p_cam.node().get_lens().set_fov(90)
    p_cam.reparent_to(p_root)

    p_char_normal = Actor('assets/bam/player.bam')
    p_char_normal.find('**/gore').hide()
    p_char_normal.reparent_to(p_root)
    p_char_normal.set_pos(0,0,-1)
    p_char_normal.set_h(90)

    p_char_hurt = Actor('assets/bam/player_hurt.bam')
    p_char_hurt.find('**/gore').hide()
    p_char_hurt.reparent_to(p_root)
    p_char_hurt.set_pos(0,0,-1)
    p_char_hurt.set_h(90)
    p_char_hurt.hide()
    p_char = p_char_normal

    p_stance = 0 # -1 bottom, 0 center, 1 top
    p_direction = 1 # -1 left, 1 right
    p_firing = [0, 0.5]
    p_reload_wait = 0
    p_reloading = 0
    p_clip = [5, 5]

    d_quad = base.loader.load_model('assets/egg/tex_holder.egg')
    d_quad.set_scale(1.6,1.6,3.2)
    d_quad.set_transparency(TransparencyAttrib.M_binary)

    d_quad.reparent_to(base.render)
    # d_quad.set_pos(0,2.2,-1.25)
    base.cam.set_pos(-40,0,0)
    d_quad.set_pos(-40,2.2,-1.25)

    d_quad.set_texture(p_buffer.get_texture())

    pc_hit_total = 0
    is_alive = True

    def pc_reset():
        PlayerCharacter.pc_hit_total = 0
        PlayerCharacter.p_reload_wait = 0
        PlayerCharacter.p_reloading = 0
        PlayerCharacter.p_clip[0] = PlayerCharacter.p_clip[1]
        PlayerCharacter.p_char.hide(); PlayerCharacter.p_char = PlayerCharacter.p_char_normal;PlayerCharacter.p_char.show()


    def pc_shoot():
        if PlayerCharacter.is_alive:
            p_firing = PlayerCharacter.p_firing
            p_clip = PlayerCharacter.p_clip
            if p_firing[0] > 0:
                return
            elif p_clip[0] <= 0:
                play_sfx("click")
                return
            PlayerCharacter.p_reload_wait = 0
            p_clip[0] -= 1
            p_char = PlayerCharacter.p_char
            p_stance = PlayerCharacter.p_stance
            p_direction = PlayerCharacter.p_direction
            d_quad = PlayerCharacter.d_quad

            play_animation(p_char, '_fire', stance=p_stance*p_direction)
            p_firing[0] = p_firing[1]
            play_sfx("shot_{}".format(random.choice("abc")), random.uniform(0.5,1.5))

            closest = [None, 999999]
            for n, n_quad in enumerate(SpawnNPC.npc_quads):
                npc = SpawnNPC.npcs[n]
                n_quad_x = n_quad.get_x(base.render)
                player_x = d_quad.get_x(render)
                player_h = p_char.get_h()
                if (n_quad_x > player_x and player_h == 90) or (n_quad_x < player_x and player_h == -90):
                    distance = abs(player_x - n_quad_x)
                    if distance < closest[1] and npc.get_python_tag("alive") and distance < 3:
                        closest = (n, distance)

            def trace(shooter_quad, dying_quad):
                f = shooter_quad.get_pos(base.render)

                if p_char.get_h() == 90:
                    f = Vec3(f[0]+.5,f[1],f[2]+3.5)
                    if dying_quad is None:
                        t = Vec3(200,2.2,random.uniform(-15,15))
                else:
                    f = Vec3(f[0]-.5,f[1],f[2]+3.5)
                    if dying_quad is None:
                        t = Vec3(-200,2.2,random.uniform(-15,15))

                if dying_quad is not None:
                    t = dying_quad.get_pos(base.render); t.z += 1.7

                base.linesegs.move_to(f)
                base.linesegs.draw_to(t)
                base.linesegs.set_thickness(8)
                line = base.render.attach_new_node(base.linesegs.create())
                line.set_transparency(True)

                def fade_line(line, task):
                    alpha = line.get_color_scale()[3]
                    if alpha <= 0:
                        line.remove_node()
                        return task.done
                    else:
                        line.set_alpha_scale(alpha-5*base.clock.dt)
                        return task.cont
                base.task_mgr.add(fade_line, extraArgs=[line], appendTask=True)

            if not closest[0] == None:
                n = closest[0]
                trace(d_quad, SpawnNPC.npc_quads[n])
                SpawnNPC.kill_npc(SpawnNPC.npcs[n], p_char.get_h())
                play_sfx("gassy_{}".format(random.choice("abc")), random.uniform(0.5,1.5))
                play_sfx("grunt_{}".format(random.choice("abcd")), random.uniform(0.5,1.5))
            else:
                trace(d_quad, None)

    base.accept('space', pc_shoot)


class Bullets:
    bullets = [base.loader.load_model('assets/egg/tex_holder.egg') for x in range(PlayerCharacter.p_clip[1])]
    texture = base.loader.load_texture('assets/icons/bullet.png')
    texture.set_magfilter(SamplerState.FT_nearest)
    for b, bullet in enumerate(bullets):
        bullet.reparent_to(aspect2d)
        bullet.set_texture(texture)
        bullet.set_scale(0.05, 0.08, 0.08)
        bullet.set_x(-0.1+b*0.1)
        bullet.set_z(0.71)
        bullet.set_transparency(True)

def bullet_update(task):
    if PlayerCharacter.is_alive:
        for b, bullet in enumerate(Bullets.bullets):
            if b >= PlayerCharacter.p_clip[0]:
                bullet.hide()
            else:
                bullet.show()
    return task.cont


def random_positions():
    n = 0
    i = 0
    while n < ENEMY_COUNT:
        chance = ((i / ((ENEMY_COUNT * 2) - 1)) * (ENEMY_MAXCHANCE - ENEMY_MINCHANCE)) + ENEMY_MINCHANCE
        if random.random() < chance:
            yield Vec3(ENEMY_OFFSET + ((i + random.random() * 0.5) * ENEMY_SPREAD / (ENEMY_COUNT * 2)), 2.2, -2)
            n += 1
        i += 1


class SpawnNPC:

    npcs = []
    npc_quads = []
    npc_aiming = []
    npc_cooldown = []
    crosshairs = []
    crosshair_textures = []

    for aim in "down", "center", "up":
        texture = base.loader.load_texture("assets/icons/aim_{}.png".format(aim))
        texture.set_magfilter(SamplerState.FT_nearest)
        crosshair_textures.append(texture)

    for x, p in enumerate(random_positions()):
        n_buffer = base.win.make_texture_buffer('npc_buff', 512, 512)
        n_root = NodePath('npc_root')

        n_cam = base.make_camera(n_buffer)
        n_cam.set_pos(0,-10,0)
        n_cam.node().get_lens().set_focal_length(10)
        n_cam.node().get_lens().set_fov(90)
        n_cam.reparent_to(n_root)

        # TODO: enemy_a and b for level 1, b and c for level 2, c and d for level 3
        a = random.choice(["a","b"])
        n_char = Actor('assets/bam/enemy_{}.bam'.format(a))
        n_char.find('**/gore').hide()
        n_char.reparent_to(n_root)
        n_char.set_pos(0,0,-6)
        n_char.set_h(random.choice([-80,80]))
        n_char.set_python_tag("alive", True)

        # enable the auto shader for character hardware skinning
        n_char.set_shader_auto()

        n_char.loop('center_idle')

        n_stance = "center"

        n_quad = base.loader.load_model('assets/egg/tex_holder.egg')
        n_quad.set_scale(1.6,1.6,3.2)

        n_quad.reparent_to(base.render)
        n_quad.set_pos(base.render, p)
        n_quad.wrt_reparent_to(Backgrounds.facility_backgrounds[0])

        n_quad.set_texture(n_buffer.get_texture())

        npcs.append(n_char)
        npc_quads.append(n_quad)

        crosshair = base.loader.load_model('assets/egg/tex_holder.egg')
        crosshair.reparent_to(n_quad)
        crosshair.set_texture(texture)
        crosshair.set_pos(0,0,0.7)
        crosshair.set_scale(0.04,0.04,0.04)
        crosshair.set_transparency(True)
        crosshair.set_color((1,0.01,0.01,1))
        crosshair.set_billboard_point_eye()
        crosshairs.append(crosshair)

        npc_aiming.append(None)
        npc_cooldown.append(0.0)

    def kill_npc(input_npc, player_h):
        input_npc.set_h(-player_h)
        input_npc.set_python_tag("alive", False)
        input_npc.set_blend(False)
        input_npc.find("**/gore").show()
        deaths = [
            "death_vanilla_a", "death_arm_shot_a",
            "death_headshot_a", "death_headshot_b",
            "death_leg_a", "death_overkill"
        ]
        input_npc.play(random.choice(deaths))

    def respawn_npcs():
        for n_char, n_quad, pos in zip(SpawnNPC.npcs, SpawnNPC.npc_quads, random_positions()):
            n_char.find('**/gore').hide()
            n_quad.set_pos(base.render, pos)
            n_char.set_python_tag("alive", True)


def swap_enemy_models(level=0):
    for x, npc in enumerate(SpawnNPC.npcs):
        a = ["ab","bc","cd","de"]
        n_char = Actor('assets/bam/enemy_{}.bam'.format(random.choice(list(a[level]))))
        n_char.find('**/gore').hide()
        n_char.set_pos(0,0,-6)
        n_char.set_h(random.choice([-80,80]))
        n_char.reparent_to(npc.parent)
        npc.detach_node()
        n_char.set_python_tag("alive", True)
        SpawnNPC.npcs[x] = n_char


class Update:

    def npc_update(task):
        if PlayerCharacter.is_alive:
            for n, n_quad in enumerate(SpawnNPC.npc_quads):
                npc = SpawnNPC.npcs[n]
                crosshair = SpawnNPC.crosshairs[n]
                if not npc.get_python_tag("alive"):
                    crosshair.hide()
                    continue
                n_quad_x = n_quad.get_x(base.render)
                player_x = PlayerCharacter.d_quad.get_x(base.render)
                distance = abs(player_x - n_quad_x)
                aim = SpawnNPC.npc_aiming[n]
                stance = PlayerCharacter.p_stance
                d = 1 if n_quad_x > player_x else -1
                npc.set_h(-80*d)
                left_side = d

                if distance > 2.5 and distance < ENEMY_AGGRO_DIST:
                    play_animation(npc, "walk", True)
                    n_quad.set_x(base.render, n_quad.get_x(base.render)-(d*base.clock.dt))
                elif not npc.get_current_anim() == "center_fire": # D'oh!
                    play_animation(npc, "center_aim", True)

                if distance < 2.8:
                    if aim == None:
                        aim = SpawnNPC.npc_aiming[n] = stance
                    crosshair.show()
                    crosshair.set_texture(SpawnNPC.crosshair_textures[aim+1])
                    SpawnNPC.npc_cooldown[n] += base.clock.dt*2
                    cooldown = SpawnNPC.npc_cooldown[n]
                    crosshair.set_alpha_scale(cooldown)
                    # fire at player
                    if cooldown > 1.5:
                        SpawnNPC.npc_cooldown[n] = 0
                        npc.play('center_fire')
                        play_sfx("shot_{}".format(random.choice("de")))
                        if aim == stance:
                            npc_trace(n_quad, PlayerCharacter.d_quad, npc, left_side)
                            PlayerCharacter.p_char.play("death_vanilla_a")
                        SpawnNPC.npc_aiming[n] = stance
                else:
                    crosshair.hide()
        return task.cont

    def bg_move(task):
        dt = base.clock.get_dt()

        cam_x = base.cam.get_x()
        for blackout in Backgrounds.blackouts:
            blackout.set_x(cam_x)

        level = ClearWorld.current_level
        if level == 0:
            for b, background in enumerate(Backgrounds.facility_backgrounds):
                offset_speed = len(Backgrounds.facility_backgrounds)-(b*16)
                background.set_x(-offset_speed*0.02*cam_x)
        elif level == 1:
            background = Backgrounds.quarters_backgrounds[0]
            offset_speed = 0.3*16
            background.set_x(-offset_speed*0.02*cam_x)
        else:
            for b, background in enumerate(Backgrounds.cave_backgrounds):
                offset_speed = len(Backgrounds.cave_backgrounds)-(b*16)
                background.set_x(-offset_speed*0.02*cam_x)

        return task.cont

    def cam_move(task):
        dt = base.clock.get_dt()

        player = PlayerCharacter.d_quad
        p_heading = PlayerCharacter.p_char.get_h()
        c_pos = base.cam.get_pos()
        p_pos_x = player.get_x()
        c_pos_x = base.cam.get_x()
        level = ClearWorld.current_level

        if level == 0:
            backgrounds = Backgrounds.facility_backgrounds
            for s, speed in enumerate((0.2, 0.17, 0.03, 0.005)):
                d = 1 if p_heading == 90 else -1
                target = p_pos_x + (0.85*d)
                curr = base.cam.get_x()
                diff = target - curr
                if (curr < target and d == 1) or (curr > target and d == -1):
                    if abs(diff) > 0.03:
                        base.cam.set_x(curr + (3*d) * dt)
                        backgrounds[s].set_x(backgrounds[s].get_x() - speed * dt)
                    else:
                        base.cam.set_x(target)
        elif level == 1:
            background = Backgrounds.quarters_backgrounds[0]
            d = 1 if p_heading == 90 else -1
            target = p_pos_x + (0.85*d)
            curr = base.cam.get_x()
            diff = target - curr
            if (curr < target and d == 1) or (curr > target and d == -1):
                if abs(diff) > 0.03:
                    base.cam.set_x(curr + (3*d) * dt)
                    background.set_x(background.get_x() - 0.2 * dt)
                else:
                    base.cam.set_x(target)

        else:
            backgrounds = Backgrounds.cave_backgrounds
            for s, speed in enumerate((0.2, 0.17, 0.03, 0.005)):
                d = 1 if p_heading == 90 else -1
                target = p_pos_x + (0.85*d)
                curr = base.cam.get_x()
                diff = target - curr
                if (curr < target and d == 1) or (curr > target and d == -1):
                    if abs(diff) > 0.03:
                        base.cam.set_x(curr + (3*d) * dt)
                        backgrounds[s].set_x(backgrounds[s].get_x() - speed * dt)
                    else:
                        base.cam.set_x(target)

        return task.cont

    def pc_move(task):
        p_stance = PlayerCharacter.p_stance
        p_direction = PlayerCharacter.p_direction
        player = PlayerCharacter.d_quad
        p_char = PlayerCharacter.p_char

        health = int(Movement.health_text)

        if health < 70 >= 25:
            Movement.text_2_node.set_color(1,0,0,1)

        if int(Movement.health_text) < 1:
            PlayerCharacter.is_alive = False

        if PlayerCharacter.is_alive:
            if player.get_x() >= -42:
                Movement.text_2.set_text("Health: " + Movement.health_text)

                if PlayerCharacter.p_stance == 1:
                    for n, n_quad in enumerate(SpawnNPC.npc_quads):
                        n_quad.set_y(base.render, 2.199)
                elif PlayerCharacter.p_stance == 0:
                    for n, n_quad in enumerate(SpawnNPC.npc_quads):
                        n_quad.set_y(base.render, 2.2)

                if PlayerCharacter.p_firing[0] > 0:
                    PlayerCharacter.p_firing[0] -= base.clock.dt
                elif base.key_map["up"]:
                    PlayerCharacter.p_stance = 1
                    p_char.clear_color()

                elif base.key_map["down"]:
                    PlayerCharacter.p_stance = -1
                    p_char.set_color((.01,0,0,1))

                else:
                    PlayerCharacter.p_stance = 0
                    p_char.clear_color()
                if not p_stance == PlayerCharacter.p_stance:
                    play_sfx("dash")
                    PlayerCharacter.p_reload_wait = 0

                player.set_z(-3.8+(p_stance*0.2)) # d_quad.set_pos()

                def move(direction):
                    PlayerCharacter.p_direction = direction
                    stance = PlayerCharacter.p_stance
                    PlayerCharacter.p_reload_wait = 0
                    if PlayerCharacter.p_firing[0] > 0:
                        speed = PLAYER_WALK_SPEED_FIRING
                        play_animation_blend(p_char, [
                            ["walk", 0.2, True],
                            ["_aim", 0.2, True],
                            ["_fire", 0.8, False],
                        ], stance=direction*stance)
                    else:
                        speed = PLAYER_WALK_SPEED_NORMAL
                        play_animation(p_char, "walk", True)
                    player.set_x(player, speed*base.clock.dt*direction)

                if base.key_map["right"]:
                    move(1)
                elif base.key_map["left"]:
                    move(-1)
                elif not PlayerCharacter.p_firing[0] > 0:
                    astance = p_stance*p_direction
                    PlayerCharacter.p_reload_wait += base.clock.dt
                    if PlayerCharacter.p_reload_wait > 1 and PlayerCharacter.p_clip[0] < PlayerCharacter.p_clip[1]:
                        play_animation(p_char, "center_reload", False, astance)
                        bwait = 0.5
                        if PlayerCharacter.p_reload_wait >= 1+bwait:
                            PlayerCharacter.p_reload_wait -= bwait
                            PlayerCharacter.p_clip[0] += 1
                            play_sfx("reload")
                    else:
                        PlayerCharacter.p_reload_wait += base.clock.dt
                        play_animation(p_char, "_aim", True, astance)
                p_char.set_h(90*PlayerCharacter.p_direction)
            else:
                player.set_x(-42)
        else:
            Movement.text_2.set_text("Health: 0")
        return task.cont

    def win_const(task):
        if -1/base.aspect2d.get_sx() > -2:
            base.cam.node().get_lens().set_focal_length(-1/base.aspect2d.get_sx() * -10)
            base.cam.node().get_lens().set_fov(-1/base.aspect2d.get_sx() * -50)

        elif -1/base.aspect2d.get_sx() < -2:
            pass

        # add a delay so that this isn't called every frame
        task.delay_time = 0.5
        return task.again

    def player_death_update(task):
        if not PlayerCharacter.is_alive:
            ClearWorld.reset_seq()
        task.delay_time = 0.5
        return task.again

    def level_complete_update(task):
        if PlayerCharacter.d_quad.get_x() > END_OF_LEVEL:  # set this to start next level seq
            next_level()
        return task.cont

class Cutscene:
    cutscene_root = render2d.attach_new_node("cutscenes")
    cutscene_root.set_z(-0.74)
    cutscene_root.set_sz(0.74)
    cutscene_root.set_sx(0.58)
    cutscene_frames = [[],[],[],[]]
    for a, amount in enumerate((7, 3, 2, 9)):
        for i in range(amount):
            quad = base.loader.load_model('assets/egg/tex_holder.egg')
            texture = base.loader.load_texture('assets/cutscenes/cutscene_{}_{}.png'.format(a+1,i))
            texture.set_format(Texture.F_srgb_alpha)
            texture.set_magfilter(SamplerState.FT_nearest)
            quad.set_texture(texture)
            quad.reparent_to(cutscene_root)
            quad.hide()
            cutscene_frames[a].append(quad)
    current_cutscene = None
    current_frame = 0

def start_cutscene(scene):
    if scene == 3:
        play_song("4")
    else:
        play_song("2")
    Cutscene.current_cutscene = scene
    Cutscene.current_frame = 0
    Cutscene.cutscene_frames[scene][0].show()

    Movement.text_1.set_text("Press Enter to Continue")
    Movement.text_1_node.set_color(.5,.5,.5,1)
    Movement.text_2.set_text('')
    Movement.text_2_node.set_color(.5,.5,.5,1)

    for x in base.aspect2d.find_all_matches('**/tex_h*'):
        x.hide()

def next_frame():
    scene = Cutscene.current_cutscene
    if not scene == None:
        frames = Cutscene.cutscene_frames
        frames[scene][Cutscene.current_frame].hide()
        Cutscene.current_frame += 1
        if Cutscene.current_frame >= len(frames[scene]):
            level_text = Movement.level_text
            Movement.text_1.set_text('')
            Movement.text_1_node.set_color(.5,.5,.5,1)
            for x in base.aspect2d.find_all_matches('**/tex_h*'):
                x.show()
            if Cutscene.current_cutscene == 3:
                sys.exit()
            Cutscene.current_cutscene = None
            start_level()
        else:
            frames[scene][Cutscene.current_frame].show()
base.accept("enter", next_frame)

start_cutscene(0) #TODO: don't overlay on game

class ClearWorld:

    level_1 = 'Level 1: Facility' # before this level trigger cutscene 1
    level_2 = 'Level 2: Crew Quarters' # end of this level triggers cutscene 2
    level_3 = 'Level 3: Martian Caves' # end of this level triggers cutscene 3
    level_4 = 'The end'
    current_level = 0

    def reset_seq():
        cw = ClearWorld
        reset_seq = Sequence()
        reset_seq.append(Func(cw.reset))
        reset_seq.append(Wait(0.25))
        reset_seq.append(Func(cw.restart))
        reset_seq.start()

    def reset():
        Movement.text_1_node.set_color(1,0,0,1)
        Movement.text_1.set_text('You died.')
        Movement.text_2.set_text('Health: 0')
        Movement.text_2_node.set_color(0.5,0,0,1)
        base.set_background_color(1,0,0,1)
        Movement.health_text = "100"
        PlayerCharacter.pc_reset()

        for x in base.render.find_all_matches('**/*'):
            if 'cam' not in str(x):
                x.hide()

        for x in base.aspect2d.find_all_matches('**/tex_h*'):
            x.hide()

    def restart():
        PlayerCharacter.is_alive = True
        base.cam.set_pos(-40,0,0)
        base.set_background_color(0,0,0,1)
        PlayerCharacter.d_quad.set_pos(-40,2.2,-1.25)

        base.main_song.play()

        level_text = Movement.level_text
        Movement.text_1.set_text('')
        Movement.text_1_node.set_color(.5,.5,.5,1)
        Movement.text_2.set_text('Health: 100')
        Movement.text_2_node.set_color(.5,.5,.5,1)

        level = ClearWorld.current_level
        for x in base.render.find_all_matches('**/*'):
            if 'cam' not in str(x):
                x.show()

        if level == 1:
            for b in Backgrounds.facility_backgrounds:
                b.hide()

        if level == 2:
            for b in Backgrounds.facility_backgrounds:
                b.hide()
            for b in Backgrounds.quarters_backgrounds:
                b.hide()

        for bg in Backgrounds.facility_backgrounds:
            bg.set_x(40)

        for bg in Backgrounds.quarters_backgrounds:
            bg.set_x(40)

        for bg in Backgrounds.cave_backgrounds:
            bg.set_x(40)

        for x in base.aspect2d.find_all_matches('**/tex_h*'):
            x.show()

        SpawnNPC.respawn_npcs()


def next_level():
    taskMgr.removeTasksMatching('*move')
    taskMgr.removeTasksMatching('*update')
    taskMgr.removeTasksMatching('*const')

    ClearWorld.current_level += 1
    base.cam.set_pos(-40,0,0)
    PlayerCharacter.d_quad.set_pos(-40,2.2,-1.25)
    start_cutscene(ClearWorld.current_level)

def start_level():
    level = ClearWorld.current_level
    taskMgr.add(Update.bg_move)
    taskMgr.add(Update.pc_move)
    taskMgr.add(bullet_update)
    taskMgr.add(Update.cam_move)
    taskMgr.add(Update.npc_update)
    taskMgr.add(Update.level_complete_update)
    taskMgr.add(Update.player_death_update)
    taskMgr.add(Update.win_const)
    if level == 0:
        play_song("1")
    elif level == 1:
        play_song("3")
        SetupBackgrounds.setup_bg('quarters')
        swap_enemy_models(1)

        for n, n_quad in enumerate(SpawnNPC.npc_quads):
            n_quad.wrt_reparent_to(Backgrounds.quarters_backgrounds[0])

        for b in Backgrounds.facility_backgrounds:
            b.hide()

        SpawnNPC.respawn_npcs()

    else:
        play_song("3")
        SetupBackgrounds.setup_bg('cave')
        swap_enemy_models(2)

        for n, n_quad in enumerate(SpawnNPC.npc_quads):
            n_quad.wrt_reparent_to(Backgrounds.cave_backgrounds[0])

        for b in Backgrounds.facility_backgrounds:
            b.hide()
        for b in Backgrounds.quarters_backgrounds:
            b.hide()

        SpawnNPC.respawn_npcs()
    PlayerCharacter.pc_reset()


base.run()
