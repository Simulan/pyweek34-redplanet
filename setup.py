from setuptools import setup

setup(
    name='martian-thirst',
    version='1.0.0',
    options={
        'build_apps': {
            'include_patterns': {
                'assets/bam/**',
                'assets/cutscenes/**',
                'assets/egg/**',
                'assets/icons/**',
                'assets/layers/**',
                'assets/music/**',
                'assets/sfx/**',
                'assets/test/**',
                'settings.prc',
                'README.md',
            },
            'gui_apps': {
                'run_game': 'main.py',
            },
            'log_filename': "$USER_APPDATA/The Martian Thirst/output.log",
            'log_append': False,
            'plugins': [
                'pandagl',
                'p3openal_audio',
            ],
        },
    }
)
